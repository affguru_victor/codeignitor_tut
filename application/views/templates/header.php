<?php
  $link = mysqli_connect("localhost","root","","news") or die("Error " . mysqli_error($link));
?>	
<html>
<head>
	<title><?php echo $title ?></title>

<style>

body {
    font-family: arial, verdana, san-serif;
    background-color: #CCFFFF;    
}

#footer {
    margin: 0 auto; 
    text-align: center;
    font-size: 12px;
}


#container {
    width: 500px;
    border: 1px solid #ccc;
    margin: 0 auto;
    padding: 10px;    
    background-color: white;
}

#nav_bar ul{
    list-style-type:none;
    margin:0 auto;
    padding:0;
    text-align: center;        
}
#nav_bar li {
    display:inline;
}

#nav_bar li a {
    text-decoration: none;
    background-color: #0099FF;
    padding: 10px;
    color: white;    
}

ul#nav_bar li a:hover {
    background-color: #00CCFF;
}


		
hr.style1 {
    margin: 15px;
    border: 0;
    height: 1px;
    background: #333;
    background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc); 
    background-image:    -moz-linear-gradient(left, #ccc, #333, #ccc); 
    background-image:     -ms-linear-gradient(left, #ccc, #333, #ccc); 
    background-image:      -o-linear-gradient(left, #ccc, #333, #ccc); 
}



</style>    
    
</head>
<body>
    <!-- START Content Container --> 
    <div id="container">

	<h1>CodeIgniter 2 Tutorial</h1>
    
    <!-- START Navigation Bar -->
    <hr class="style1" />
    <div id="nav_bar">
        <ul>
            <li><a href="<?php echo base_url() ?>">Home</a></li>
            <li><a href="<?php echo base_url('index.php/news/create') ?>">Create Article</a></li>
            <li><a href="<?php echo base_url('index.php/news/') ?>">News Items</a></li>
        </ul>
    </div>
    <hr class="style1" />        
    <!-- END Navigation Bar -->    
    
